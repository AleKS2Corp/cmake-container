FROM gcc:latest

ADD cmake_install.sh /

RUN chmod +x /cmake_install.sh
RUN /cmake_install.sh

CMD ["/bin/bash"]