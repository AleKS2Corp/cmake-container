#!/bin/sh

wget https://github.com/Kitware/CMake/releases/download/v3.23.1/cmake-3.23.1.tar.gz
tar xvf cmake-3.23.1.tar.gz
cd cmake-3.23.1
./bootstrap --system-curl
gmake
make install
cd ..
apt-get update && apt-get install -y libgtest-dev libboost-test-dev && rm -rf /var/lib/apt/lists/*