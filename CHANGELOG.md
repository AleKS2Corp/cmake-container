# Changelog

## 3.23.1 (2022-04-25)

#### New Features

* :sparkles: cmake 3.23.1
#### Others

* :green_heart: YAML template
